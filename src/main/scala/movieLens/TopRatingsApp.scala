package movieLens

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac058 on 2017/5/15.
  */
object TopRatingsApp extends App{

  val conf = new SparkConf().setAppName("TopRatings")
    .setMaster("local[*]")
  val sc = new SparkContext(conf)

  val ratings:RDD[(Int,Double)]=sc.textFile("/Users/mac058/Downloads/ml-20m/ratings.csv")
    .map(str=>str.split(","))
      .filter(strs=>strs(1)!="movieId")     //檔案第一列為名稱，run會錯誤，所以將其刪除才可順利轉換//
    .map(strs=>{
//      println(strs.mkString(","))         //取樣時未註解掉會造成錯誤
      (strs(1).toInt,strs(2).toDouble)
    })

//  ratings.take(5).foreach(println)

  val totalRatingByMovieId=ratings.reduceByKey((acc,curr)=>acc+curr)
   totalRatingByMovieId.takeSample(false,5).foreach(println)            //抽樣（取後不放回)

//  val averageRatingByMovieId:RDD[(Int,(Double,Int))]=ratings.mapValues(v=>v->1)
  val averageRatingByMovieId:RDD[(Int,Double)]=ratings.mapValues(v=>v->1)
    .reduceByKey((acc,curr)=>{                  //curr是當下的值，acc是累積至當下之前的值
      (acc._1+curr._1)->(acc._2+curr._2)        //._1是Double，._2是Int
    }).mapValues(kv=>kv._1/kv._2.toDouble)
  averageRatingByMovieId.takeSample(false,5).foreach(println)

  val top10=averageRatingByMovieId.sortBy(_._2,false).take(10)
  top10.foreach(println)

  val movies:RDD[(Int,String)]=sc.textFile("/Users/mac058/Downloads/ml-20m/movies.csv")
    .map(str=>str.split(","))
      .filter(strs=>strs(0)!="movieId")
    .map(strs=>{
//      println(strs.mkString(","))
      (strs(0).toInt,strs(1))      //位置從０開始，另外strs(1)本身為string，所以不需作轉換
    })
  movies.take(5).foreach(println)

  val joined:RDD[(Int,(Double,String))]=averageRatingByMovieId.join(movies)     //Int=movieId，Double=平均，String=movies
  joined.takeSample(false, 5).foreach(println)                 //找出資料一樣的部分（平均與電影）
  joined.map(v=>v._1+","+v._2._2+","+v._2._1).saveAsTextFile("result")                //存成資料並轉成csv
//  sc.textFile("result").map(str=>str.split(","))
    .map(strs=>strs(0).toInt->strs(1)->strs(2).toDouble)         //讀檔

}
